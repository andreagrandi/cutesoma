﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace CuteSoma
{
    public class SomaChannel
    {
        public string channelID { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string dj { get; set; }
        public string djMail { get; set; }
        public string genre { get; set; }
        public string image { get; set; }
        public string imageLarge { get; set; }
        public string imageXL { get; set; }
        public string updatedTime { get; set; }
        public string playlistFastMP3 { get; set; }
        public string playlistSlowMP3 { get; set; }
        public string playlistFastAACP { get; set; }
        public string playlistSlowAACP { get; set; }
        public string lastPlaying { get; set; }
        public string listeners { get; set; }
    }
}
