﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Windows.Media.Imaging;
using SomaPlaybackAgent;
using Microsoft.Phone.BackgroundAudio;
using System.Text.RegularExpressions;
using Microsoft.Phone.Tasks;

namespace CuteSoma
{
    public class SomaChannelTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Available
        {
            get; set;
        }

        public DataTemplate Unavailable
        {
            get; set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            SomaChannel channel = item as SomaChannel;
            if (channel != null)
            {
                if ((Application.Current as App).IsTrial)
                {
                    if ((Application.Current as App).AvailableChannels.Contains(channel.channelID))
                    {
                        return Available;
                    }
                    else
                    {
                        return Unavailable;
                    }
                }
                else
                    return Available;
            }

            return base.SelectTemplate(item, container);
        }
    }

    public partial class MainPage : PhoneApplicationPage
    {
        private SomaChannel sItem;
        private List<SomaChannel> items = new List<SomaChannel>();
        string lastPlayedChannel = string.Empty;
        bool isActive = false;
        MarketplaceDetailTask _marketPlaceDetailTask = new MarketplaceDetailTask();

        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("lastPlayedChannel"))
                lastPlayedChannel = (string)PhoneApplicationService.Current.State["lastPlayedChannel"];
            if (PhoneApplicationService.Current.State.ContainsKey("isActive"))
                isActive = (bool)PhoneApplicationService.Current.State["isActive"];
            Debug.WriteLine("Is Active: " + isActive.ToString());
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            PhoneApplicationService.Current.State["isActive"] = true;
        }

        private void GetChannels(object sender, RoutedEventArgs e)
        {
            if (!isActive)
            {
                WebClient webClient = new WebClient();
                webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
                webClient.DownloadStringAsync(new System.Uri("http://somafm.com/channels.xml"));
            }
        }

        private void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Debug.WriteLine("Error during channels.xml fetching");
                    MessageBox.Show(e.Error.Message);
                });
            }
            else
            {
                UpdateFeedList(e.Result);
            }
        }

        private void UpdateFeedList(string channelsXML)
        {
            StringReader stringReader = new StringReader(channelsXML);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            XDocument doc = XDocument.Load(xmlReader);

            XElement emptyElement = XElement.Parse("<x></x>");

            List<string> bannedChannels = new List<string> { "sf1033", "xmasrocks" };

            foreach (XElement results in doc.Descendants("channel"))
            {
                SomaChannel item = new SomaChannel();
                item.channelID = results.Attribute("id").Value.ToString();
                item.title = (results.Element("title") ?? emptyElement).Value.ToString();
                item.description = (results.Element("description") ?? emptyElement).Value.ToString();
                item.dj = (results.Element("dj") ?? emptyElement).Value.ToString();
                item.djMail = (results.Element("djmail") ?? emptyElement).Value.ToString();
                item.genre = (results.Element("genre") ?? emptyElement).Value.ToString();
                item.image = (results.Element("image") ?? emptyElement).Value.ToString();
                item.imageLarge = (results.Element("largeimage") ?? emptyElement).Value.ToString();
                item.imageXL = (results.Element("xlimage") ?? emptyElement).Value.ToString();
                item.updatedTime = (results.Element("updated") ?? emptyElement).Value.ToString();
                item.playlistFastAACP = (results.Elements("fastpls").Where(a => a.Attribute("format").Value == "aacp")
                    .Select(a => a.Value)).FirstOrDefault();
                item.playlistSlowAACP = (results.Elements("slowpls").Where(a => a.Attribute("format").Value == "aacp")
                    .Select(a => a.Value)).FirstOrDefault();
                item.playlistFastMP3 = (results.Elements("fastpls").Where(a => a.Attribute("format").Value == "mp3")
                    .Select(a => a.Value)).FirstOrDefault();
                item.playlistSlowMP3 = (results.Elements("slowpls").Where(a => a.Attribute("format").Value == "mp3")
                    .Select(a => a.Value)).FirstOrDefault();
                item.lastPlaying = (results.Element("lastPlaying") ?? emptyElement).Value.ToString();
                item.listeners = (results.Element("listeners") ?? emptyElement).Value.ToString();
                
#if WP8
                if(!bannedChannels.Contains(item.channelID))
                    items.Add(item);
#else
                items.Add(item);
#endif
            }

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                channelsList.ItemsSource = items;
                progressBar.IsIndeterminate = false;
                progressBar.Visibility = Visibility.Collapsed;
            });
        }

        private void LoadChannel(object sender)
        {
            ListBox listBox = sender as ListBox;

            if (listBox != null && listBox.SelectedItem != null)
            {
                sItem = (SomaChannel)listBox.SelectedItem;

                if (PhoneApplicationService.Current.State.ContainsKey("lastPlayedChannel"))
                    lastPlayedChannel = (string)PhoneApplicationService.Current.State["lastPlayedChannel"];

                if (sItem.channelID == lastPlayedChannel)
                {
                    string pageUrl = "/SomaPlaying.xaml?channelid=" + sItem.channelID + "&newchannel=0";
                    NavigationService.Navigate(new Uri(pageUrl, UriKind.Relative));
                }
                else
                {
                    if (!string.IsNullOrEmpty(sItem.playlistFastMP3))
                    {
                        SomaPlaylistParser spp = new SomaPlaylistParser();
                        spp.PlaylistParsed += new SomaPlaylistEventHandler(spp_playListParsed);
                        spp.Parse(sItem.playlistFastMP3);
                    }
                }
            }
        }

        private void spp_playListParsed(object sender, SomaPlaylistEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Playlist.Url))
            {
                AudioTrack track = BackgroundAudioPlayer.Instance.Track;
                track = new AudioTrack();

                track.BeginEdit();

                if (!string.IsNullOrEmpty(sItem.lastPlaying))
                {
                    string[] i = sItem.lastPlaying.Split(new string[] { "-" }, StringSplitOptions.None);
                    track.Artist = i[0].Trim();
                    track.Title = i[1].Trim();
                }

                if (Regex.IsMatch(e.Playlist.Url, ":[0-9]")) // WORKAROUND: this fixes problems with urls ending with port number
                    track.Source = new Uri(e.Playlist.Url + "/; stream.mp3", UriKind.Absolute);
                else
                    track.Source = new Uri(e.Playlist.Url, UriKind.Absolute);

                track.EndEdit();

                BackgroundAudioPlayer.Instance.Track = track;
                BackgroundAudioPlayer.Instance.Play();

                string imageUrl;

                if (!string.IsNullOrEmpty(sItem.imageLarge))
                    imageUrl = sItem.imageLarge;
                else
                {
                    if (!string.IsNullOrEmpty(sItem.imageXL))
                        imageUrl = sItem.imageXL;
                    else
                        imageUrl = sItem.image;
                }

                if (!string.IsNullOrEmpty(imageUrl))
                    PhoneApplicationService.Current.State["playingImage"] = imageUrl;

                PhoneApplicationService.Current.State["channelTitle"] = sItem.title;
                PhoneApplicationService.Current.State["lastPlayedChannel"] = sItem.channelID;

                string pageUrl = "/SomaPlaying.xaml?channelid=" + sItem.channelID + "&newchannel=1";
                NavigationService.Navigate(new Uri(pageUrl, UriKind.Relative));
            }
            else
                MessageBox.Show("Radio URL not found!");
        }

        private void channelTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            SomaChannel channel = (SomaChannel)listBox.SelectedItem;

            if ((Application.Current as App).IsTrial)
                if ((Application.Current as App).AvailableChannels.Contains(channel.channelID))
                    LoadChannel(sender);
                else
                {
                    string message = channel.title + " is not available in Trial mode. Do you want to purchase the full version?";

                    if (MessageBox.Show(message, "Buy CuteSoma", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        _marketPlaceDetailTask.Show();
                    }
                }
            else
                LoadChannel(sender);
        }

        private void ShowAbout(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/SomaAbout.xaml", UriKind.Relative));
        }
    }
}