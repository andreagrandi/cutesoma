﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CuteSoma")]
[assembly: AssemblyDescription("Soma.fm client")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Andrea Grandi")]
[assembly: AssemblyProduct("CuteSoma")]
[assembly: AssemblyCopyright("Copyright © 2012 - Andrea Grandi")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5a629dfe-346c-435f-98ab-511a3e2a13be")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.9.*")]
[assembly: AssemblyFileVersion("1.0.9.*")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
