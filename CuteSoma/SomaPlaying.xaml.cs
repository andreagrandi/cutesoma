﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.BackgroundAudio;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Windows.Threading;

namespace CuteSoma
{
    public partial class SomaPlaying : PhoneApplicationPage
    {
        string lastPlayedChannel = string.Empty;
        DispatcherTimer _dt;

        public SomaPlaying()
        {
            InitializeComponent();
            (Application.Current as App).ChannelInfoUpdate += new SomaChannelInfoEventHandler(ChannelInfoUpdated);

            _dt = new DispatcherTimer();
            _dt.Interval = new TimeSpan(0, 0, 10); // 10 seconds
            _dt.Tick += new EventHandler(HideProgressBar);

            if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Playing)
                playPauseButton.IsChecked = true;
            else
                playPauseButton.IsChecked = false;
        }

        private void HideProgressBar(object sender, EventArgs e)
        {
            progressBar.Visibility = Visibility.Collapsed;
            playPauseButton.IsEnabled = true;
            playPauseButton.Opacity = 1;
            _dt.Stop();
        }

        private void ChannelInfoUpdated(object sender, SomaChannelInfoEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("lastPlayedChannel"))
                lastPlayedChannel = (string)PhoneApplicationService.Current.State["lastPlayedChannel"];

            foreach (SomaChannel c in e.Channels)
            {
                if (c.channelID == lastPlayedChannel)
                {
                    Debug.WriteLine("Current song: " + c.lastPlaying);
                    
                    if (!string.IsNullOrEmpty(c.lastPlaying))
                    {
                        string[] i = c.lastPlaying.Split(new string[] { "-" }, StringSplitOptions.None);
                        titleText.Text = i[1].Trim();
                        artistText.Text = i[0].Trim();
                    }
                }
            }
        }

        private void playPauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (BackgroundAudioPlayer.Instance.PlayerState == PlayState.Playing)
            {
                BackgroundAudioPlayer.Instance.Stop();
                playPauseButton.IsChecked = false;
            }
            else
            {
                BackgroundAudioPlayer.Instance.Play();
                playPauseButton.IsChecked = true;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (PhoneApplicationService.Current.State.ContainsKey("lastPlayedChannel"))
                lastPlayedChannel = (string)PhoneApplicationService.Current.State["lastPlayedChannel"];

            string playingImage = string.Empty;
            if(PhoneApplicationService.Current.State.ContainsKey("playingImage"))
                playingImage = (string)PhoneApplicationService.Current.State["playingImage"];

            titleText.Text = BackgroundAudioPlayer.Instance.Track.Title;
            artistText.Text = BackgroundAudioPlayer.Instance.Track.Artist;

            if (!string.IsNullOrEmpty(playingImage))
            {
                channelImage.Source = new BitmapImage(new Uri(playingImage, UriKind.RelativeOrAbsolute));
            }

            if (!string.IsNullOrEmpty((string)PhoneApplicationService.Current.State["channelTitle"]))
            {
                channelTitleText.Text = "Soma.fm - " + (string)PhoneApplicationService.Current.State["channelTitle"];
            }

            string newchannel;

            if (NavigationContext.QueryString.TryGetValue("newchannel", out newchannel))
            {
                if (!string.IsNullOrEmpty(newchannel))
                {
                    if (newchannel == "1")
                    {
                        progressBar.Visibility = Visibility.Visible;
                        playPauseButton.IsChecked = true;
                        playPauseButton.IsEnabled = false;
                        playPauseButton.Opacity = 0.5;
                        _dt.Start();
                    }
                }
            }
        }
    }
}