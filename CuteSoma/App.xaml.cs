﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.BackgroundAudio;
using System.Windows.Threading;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Diagnostics;
using Microsoft.Phone.Marketplace;

namespace CuteSoma
{
    public class SomaChannelInfoEventArgs : EventArgs
    {
        public List<SomaChannel> Channels { get; set; }
    }

    public delegate void SomaChannelInfoEventHandler(object sender, SomaChannelInfoEventArgs e);

    public partial class App : Application
    {
        public event SomaChannelInfoEventHandler ChannelInfoUpdate;

        protected virtual void OnChannelInfoUpdate(SomaChannelInfoEventArgs e)
        {
            if (ChannelInfoUpdate != null)
            {
                ChannelInfoUpdate(this, e);
            }
        }

        DispatcherTimer _dt;

        // WORKAROUND: this line is needed by the Windows Store, else it cannot detect ID_CAP_MEDIALIB capability
        Microsoft.Xna.Framework.Media.MediaLibrary library = new Microsoft.Xna.Framework.Media.MediaLibrary();

        // LICENSE
        private static LicenseInformation _licenseInfo = new LicenseInformation();

        private static bool _isTrial = true;
        
        public bool IsTrial
        {
            get
            {
                return _isTrial;
            }
        }

        /// <summary>
        /// Check the current license information for this application
        /// </summary>
        private void CheckLicense()
        {
            // When debugging, we want to simulate a trial mode experience. The following conditional allows us to set the _isTrial 
            // property to simulate trial mode being on or off. 
#if DEBUG
            string message = "This sample demonstrates the implementation of a trial mode in an application." +
                               "Press 'OK' to simulate trial mode. Press 'Cancel' to run the application in normal mode.";
         
            if (MessageBox.Show(message, "Debug Trial",
                 MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                _isTrial = true;
            }
            else
            {
                _isTrial = false;
            }
#else
            _isTrial = _licenseInfo.IsTrial();
#endif
        }

        // Channels available in the TRIAL mode
        private List<string> _availableChannels = new List<string> { "secretagent", "beatblender", 
            "illstreet", "xmasinfrisko", "christmas", "xmasrocks" };

        public List<string> AvailableChannels
        {
            get { return _availableChannels; }
        }

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public PhoneApplicationFrame RootFrame { get; private set; }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = false;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

            _dt = new DispatcherTimer();
            _dt.Interval = new TimeSpan(0, 0, 10);
            _dt.Tick += new EventHandler(UpdateChannels);
        }

        private void UpdateChannels(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_UpdateChannelsCompleted);
            webClient.DownloadStringAsync(new System.Uri("http://somafm.com/refresh.xml?rand="
                + DateTime.Now.Ticks.ToString()));  // WORKAROUND to disable WebClient cache (appending a random parameter to the URL)
        }

        private void webClient_UpdateChannelsCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                Debug.WriteLine(e.Error.ToString());
            }
            else
            {
                StringReader stringReader = new StringReader(e.Result);
                XmlReader xmlReader = XmlReader.Create(stringReader);
                XDocument doc = XDocument.Load(xmlReader);

                List<SomaChannel> channels = new List<SomaChannel>();

                foreach (XElement item in doc.Descendants("channel"))
                {
                    SomaChannel c = new SomaChannel();
                    c.channelID = item.Attribute("id").Value.ToString();
                    c.lastPlaying = item.Element("lastPlaying").Value.ToString();
                    c.listeners = item.Element("listeners").Value.ToString();
                    channels.Add(c);
                }

                SomaChannelInfoEventArgs sci = new SomaChannelInfoEventArgs();
                sci.Channels = channels;
                OnChannelInfoUpdate(sci);
            }
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            ThemeManager.ToDarkTheme(); // Force the use of dark theme
            CheckLicense();
            _dt.Start();
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            CheckLicense();
            _dt.Start();
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            _dt.Stop();
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}