﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;

namespace CuteSoma
{
    public class SomaPlaylistEventArgs : EventArgs 
    {
        public SomaPlaylist Playlist { get; set; }
    }

    public delegate void SomaPlaylistEventHandler(object sender, SomaPlaylistEventArgs e);

    class SomaPlaylistParser
    {
        public event SomaPlaylistEventHandler PlaylistParsed;

        protected virtual void OnPlaylistParsed(SomaPlaylistEventArgs e)
        {
            if (PlaylistParsed != null)
            {
                PlaylistParsed(this, e);
            }
        }

        public void Parse(string pls)
        {
            WebClient webClient = new WebClient();
            webClient.DownloadStringCompleted += new DownloadStringCompletedEventHandler(webClient_DownloadStringCompleted);
            webClient.DownloadStringAsync(new System.Uri(pls));
        }

        private void webClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            string playlist = e.Result;
            string[] lines = playlist.Split(new string[] {"\n"}, StringSplitOptions.RemoveEmptyEntries);

            SomaPlaylist p = new SomaPlaylist();

            foreach (string line in lines)
            {
                if (Regex.IsMatch(line, "File[0-9]+=http://"))  // Use the latest URL available. Firewall friendly if available or second one
                {
                    int position = line.IndexOf("=");
                    p.Url = line.Substring(position + 1);
                }
            }

            SomaPlaylistEventArgs spe = new SomaPlaylistEventArgs();
            spe.Playlist = p;
            OnPlaylistParsed(spe);
        }
    }
}
